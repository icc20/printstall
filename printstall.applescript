var app = Application.currentApplication();
app.includeStandardAdditions = true;

var lpinfo = JSON.parse(app.doShellScript('curl https://print.gen.cam.ac.uk/drivers/mac/lpinfo.json'));

var drivers = JSON.parse(app.doShellScript('curl https://print.gen.cam.ac.uk/drivers/mac/drivers.json'));

var printers = app.chooseFromList(Object.keys(lpinfo), {
    withPrompt: "Select printers to install:",
	multipleSelectionsAllowed: true
});

if(!Array.isArray(printers)){
	printers=[printers];
}

printers.forEach(function(p) {
	if(!p) {
		app.displayDialog("No printer selected, nothing to do",{
			buttons:["Quit"],
		});
		return;
	}
	// Check existence & md5 of ppd
	try {
		var resp=app.doShellScript('md5 "'+lpinfo[p].ppd+'"');
		var md5=resp.split(' = ')[1];
		if(md5 != lpinfo[p].md5) {
			getdriver(lpinfo[p].ppd);
		}
	} catch (e) {
		getdriver(lpinfo[p].ppd);
	}
	Progress.description="Installing printer: "+p;
	Progress.totalUnitCount=2;
	
	// Remove any printer which might clash
	Progress.additionalDescription="Removing old printer";
	Progress.completedUnitCount=0;
	delay(1);
	app.doShellScript('lpadmin -x '+p+'; true', {
		administratorPrivileges: true
	});

	// Install printer
	Progress.completedUnitCount=1;
	Progress.additionalDescription="Adding printer";
	delay(1);
	app.doShellScript('lpadmin -p '+p+
		' -m "'+lpinfo[p].ppd+
		'" -v "'+lpinfo[p].url+
		'" -L "'+lpinfo[p].loc+
		'" -E -o printer-is-shared=false -o auth-info-required=username,password '+
		lpinfo[p].opt,{  
			administratorPrivileges: true 
	});

	Progress.completedUnitCount=2;
	Progress.additionalDescription="Configuring options";
	delay(1);
	// PPD level options don't get set on 10.10, edit the PPD directly
	var sysinf=app.systemInfo();
	if(sysinf.systemVersion.indexOf('10.10')==0 && lpinfo[p].sed) {
		app.doShellScript("sed -E -i '' -e '"+lpinfo[p].sed+"' /etc/cups/ppd/"+p+".ppd",{ 
			administratorPrivileges: true 
		});
	}
});

function getdriver(ppd) {
	// make a safe place to work
	var tmpdir=app.doShellScript("mktemp -d -t printstall");
	Progress.description="Installing driver:"+drivers[ppd].dsc;
	Progress.totalUnitCount=3;
	
	// Download the zip
	Progress.completedUnitCount=0;
	Progress.additionalDescription="Downloading ZIP";
	delay(1);
	app.doShellScript("cd "+tmpdir+" && curl -o driver.zip "+drivers[ppd].zip);
	
	// Extract the zip
	Progress.completedUnitCount=1;
	Progress.additionalDescription="Extracting ZIP";
	delay(1);
	app.doShellScript("cd "+tmpdir+" && ditto -xk driver.zip .");
	
	// Run the installer
	Progress.completedUnitCount=2;
	Progress.additionalDescription="Installing driver package";
	delay(1);
	app.displayDialog(drivers[ppd].msg,{
		buttons:["Continue"],
		//givingUpAfter:10
	});
	app.doShellScript('installer -pkg '+tmpdir+'/"'+drivers[ppd].pkg+'" -tgt /',{
		administratorPrivileges: true
	}); 
	
	// Remove zip and installer
	Progress.completedUnitCount=3;
	Progress.additionalDescription="Tidying up";
	delay(1);
	app.doShellScript("rm -rf "+tmpdir);
}
